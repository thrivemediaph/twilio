<?php

use App\Http\Controllers\Api\CallController;
use App\Http\Controllers\Api\RecordingController;
use App\Http\Controllers\Api\SmsController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [UserController::class, 'register']);
Route::post('/login', [UserController::class, 'login']);

Route::get('/calls', [CallController::class, 'get_call_list']);
Route::get('/calls/{call_sid}', [CallController::class, 'get_call_by_callsid']);
Route::post('/calls/create', [CallController::class, 'create_call']);
Route::post('/calls/outbound', [CallController::class, 'make_outbound_call']);
Route::post('/calls/receive/inbound', [CallController::class, 'receive_inbound_call']);

Route::post('/recordings/calls/outbound', [RecordingController::class, 'make_outbound_call_with_recording']);
Route::post('/recordings/create ', [RecordingController::class, 'create_recording']);

Route::get('/sms/list/all', [SmsController::class, 'listMessages']);
Route::post('/sms/respond', [SmsController::class, 'receiveSMS']);
Route::post('/sms/question/respond', [SmsController::class, 'respondToQuestion']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
