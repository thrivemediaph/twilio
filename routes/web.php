<?php

use App\Http\Controllers\SMSController;
use App\Http\Controllers\VoiceCallController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sms/', [SMSController::class, 'show'])->name('sms_index');
Route::get('/sms/send_sms', [SMSController::class, 'sendMessageForm'])->name('send_sms_form');
Route::post('/sms/send_sms/custom', [SMSController::class, 'sendCustomMessage'])->name('send_custom_sms');

Route::get('/call_index/', [VoiceCallController::class, 'show'])->name('call_index');
Route::get('/call', [VoiceCallController::class, 'voiceCall'])->name('voiceCallForm');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
