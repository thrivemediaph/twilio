@extends('layouts.app')

@section('title')
    Twilio Test
@endsection

@section('content')
    <div class="container text-center">
        <div class="row">
            <div class="col">
                Twilio Test
            </div>
            <div class="col text-muted">
                Laravel Build v{{ Illuminate\Foundation\Application::VERSION }}
            </div>
        </div>
    </div>
@endsection
