@extends('layouts.app')

@section('title')
Call Logs
@endsection

@section('content')
<div class="container">
    <h2>Call Logs</h2>
    <div class="text-right mb-5">
        <a href="{{ route('voiceCallForm') }}"><button class="btn btn-primary">Send Call</button></a>
    </div>
    @if (count($calls) > 0)
    <table class="table table-responsive-lg">
        <thead>
            <th>Call ID</th>
            <th>Sender</th>
            <th>Receiver</th>
            <th>Call Date</th>
            <th>Call Time</th>
            <th>Duration</th>
            <th>Recording</th>
        </thead>
        <tbody>
        @foreach ($calls as $call)
            <tr>
                <td>{{ $call['twilio_call_sid'] }}</td>
                <td>{{ $call['from_number'] }}</td>
                <td>{{ $call['to_number'] }}</td>
                @if ($call['start_time'] == null)
                    <td colspan="2" class="text-center">Call failed or was rejected</td>
                @else
                    <td>{{ \Carbon\Carbon::parse($call['start_time'])->format('M j, Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($call['start_time'])->format('H:i:s') }} - {{ \Carbon\Carbon::parse($call['end_time'])->format('H:i:s') }}</td>
                @endif
                <td>{{ $call['duration'] }}</td>
                @if ($call['recording_url'] != null)
                    <td><a href="{{ $call['recording_url'] }}">Listen</a></td>
                @else
                    <td><i>No recording found</i></td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
    @else
        <p>There are currently no call logs to display.</p>
    @endif
</div>
@endsection
