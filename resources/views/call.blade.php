@extends('layouts.app')

<link rel="stylesheet" href="site.css">

@section('title')
Send and Receive Calls
@endsection

@section('content')
<div class="jumbotron">
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif
    <div id="incoming-connection-alert" class="alert alert-info" style="display: none;" role="alert">
        Incoming connection! Will you answer? <a href="#" id="incoming-connection-yes">Yes</a> / <a href="#" id="incoming-connection-no">No</a>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">Phone Call</div>
                <div class="card-body">
                    <div id="controls">
                        <div id="info">
                            <p class="instructions">Twilio Client</p>
                            <div id="client-name"></div>
                                <div id="output-selection">
                                    <label>Ringtone Devices</label>
                                    <select id="ringtone-devices" multiple></select>
                                    <label>Speaker Devices</label>
                                    <select id="speaker-devices" multiple></select><br/>
                                    <a id="get-devices">Seeing unknown devices?</a>
                                </div>
                            </div>
                            <div id="call-controls">
                                <p class="instructions">Make a Call:</p>
                                <input id="phone-number" type="text" placeholder="Enter a phone # or client name" />
                                <button id="button-call" >Call</button>
                                <button id="button-hangup" >Hangup</button>
                                <div id="volume-indicators">
                                    <label>Mic Volume</label>
                                    <div id="input-volume"></div><br/><br/>
                                    <label>Speaker Volume</label>
                                    <div id="output-volume"></div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-header">Logs</div>
                <div class="card-body">
                        <div id="log"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="twilio.js"></script>
{{--<script type="text/javascript" src="twilio.min.js"></script>--}}
<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="quickstart.js"></script>
@endsection
