@extends('layouts.app')

@section('title')
SMS Logs
@endsection

@section('content')
<div class="container">
    <h2>SMS Logs</h2>
    <div class="text-right mb-5">
        <a href="{{ route('send_sms_form') }}"><button class="btn btn-primary">Send SMS</button></a>
    </div>
    @if ($sms->count() > 0)
    <table class="table table-responsive-lg">
        <thead>
            <th>Message ID</th>
            <th>Sender</th>
            <th>Receiver</th>
            <th>Message</th>
            <th># of Segments</th>
            <th>SMS Date</th>
        </thead>
        <tbody>
        @foreach ($sms as $message)
            <tr>
                <td>{{ $message->sms_message_id }}</td>
                <td>{{ $message->sender_number }}</td>
                <td>{{ $message->receiver_number }}</td>
                <td>{{ $message->message }}</td>
                <td>{{ $message->no_of_segments }}</td>
                <td>{{ \Carbon\Carbon::parse($message->created_at)->format('M j, Y') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @else
        <p>There are currently no SMS logs to display.</p>
    @endif
</div>
@endsection
