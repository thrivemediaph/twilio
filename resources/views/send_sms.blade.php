@extends('layouts.app')

@section('title')
Send SMS
@endsection

@section('content')
<div class="jumbotron">
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">Send SMS message</div>
                <div class="card-body">
                    <div class="mb-2">
                        <p>Sample text format to answer codes:</p>
                        <i>CODE/QUESTION CODE/FULL NAME send to +17273491007.</i>
                        <p>ex. CODE/123456/KEVIN DECENA</p>
                    </div>
                    <form method="POST" action="{{ route('send_custom_sms') }}">
                    @csrf
                        <div class="form-group">
                            <label>Select users as recipients</label>
                            <select name="users[]" multiple class="form-control">
                            @foreach ($users as $user)
                                <option>{{ $user->mobile_number }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Notification Message</label>
                            <textarea name="body" class="form-control" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="add_question" id="add_question" {{ old('add_question') ? 'checked' : '' }}>

                                <label class="form-check-label" for="add_question">Add to Questions table?</label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Send Custom SMS</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
