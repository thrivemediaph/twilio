<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = ['first_name', 'middle_name', 'last_name', 'full_name', 'mobile_number', 'password', 'email'];
    protected $hidden = ['password', 'remember_token'];
    protected $casts = ['email_verified_at' => 'datetime'];

    /**
     * Set the user's first_name, middle_name, and last_name values to uppercase. - STANDARD.
     * Set the user's username, and email values to lowercase - STANDARD.
     */
    public function setFullNameAttribute () {
        $this->attributes['full_name'] = Str::title($this->attributes['first_name']. ' ' .$this->attributes['middle_name']. ' ' .$this->attributes['last_name']);
    }

    public function setFirstNameAttribute ($value) {
        $this->attributes['first_name'] = strtoupper($value);
    }

    public function setMiddleNameAttribute ($value) {
        $this->attributes['middle_name'] = strtoupper($value);
    }

    public function setLastNameAttribute ($value) {
        $this->attributes['last_name'] = strtoupper($value);
    }

    public function setEmailAttribute ($value) {
        $this->attributes['email'] = strtolower($value);
    }
}
