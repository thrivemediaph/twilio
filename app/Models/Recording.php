<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recording extends Model
{
    use HasFactory;

    protected $fillable = ['twilio_recording_sid', 'twilio_call_sid', 'recording_url', 'start_time', 'duration'];

    /** Append seconds on duration input
     */
    public function setDurationAttribute ($value) {
        $this->attributes['duration'] = $value. " seconds";
    }
}
