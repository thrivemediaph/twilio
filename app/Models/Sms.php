<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    use HasFactory;

    protected $fillable = ['sms_message_id', 'sender_number', 'receiver_number', 'message', 'no_of_segments'];
}
