<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    use HasFactory;

    protected $fillable = ['twilio_call_sid', 'from_number', 'to_number', 'start_time', 'end_time', 'duration', 'type'];

    /** Append seconds on duration input
     */
    public function setDurationAttribute ($value) {
        $this->attributes['duration'] = $value. " seconds";
    }
}
