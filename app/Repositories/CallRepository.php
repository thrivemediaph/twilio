<?php

namespace App\Repositories;

/* Models */
use App\Models\Call;

/* Core */
use App\Interfaces\CallInterface;
use App\Services\TwilioService\GetEnvSettings;
use App\Traits\ResponseAPI;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Twilio\Jwt\ClientToken;
use Twilio\TwiML\VoiceResponse;

class CallRepository implements CallInterface
{
    // Use ResponseAPI trait in this repository
    use ResponseAPI;

    public function get_call_list () { // https://www.twilio.com/console/api-explorer/voice/calls/read
        // Get .env settings
        $get_env_settings = new GetEnvSettings();
        $client = $get_env_settings->getEnvSettings();

        $calls = $client->calls->read([]);

        $calls_array = array();

        for ($i = 0; $i < count($calls); $i++) {
            $calls_array[$i]['call_sid'] = $calls[$i]->sid;
            $calls_array[$i]['from'] = $calls[$i]->from;
            $calls_array[$i]['to'] = $calls[$i]->to; // recipient
            $calls_array[$i]['status'] = $calls[$i]->status;
            $calls_array[$i]['start_time'] = Carbon::parse($calls[$i]->startTime)->toDateTimeString();
            $calls_array[$i]['end_time'] = Carbon::parse($calls[$i]->endTime)->toDateTimeString();
            $calls_array[$i]['duration'] = $calls[$i]->duration;
            $calls_array[$i]['direction'] = $calls[$i]->direction; // inbound or outbound

            if ($calls[$i]->price) {
                $calls_array[$i]['price'] = $calls[$i]->price . " " . $calls[$i]->priceUnit;
            } elseif ($calls[$i]->price == null) {
                $calls_array[$i]['price'] = 0;
            }
        }

        return $this->success("All calls", $calls_array);
    }

    public function get_call_by_callsid ($call_sid) {
        try {
            // Get .env settings
            $get_env_settings = new GetEnvSettings();
            $client = $get_env_settings->getEnvSettings();

            $call = $client->calls($call_sid)->fetch();
            $call_array = array();

            $call_array['call_sid'] = $call->sid;
            $call_array['from'] = $call->from;
            $call_array['to'] = $call->to; // recipient
            $call_array['status'] = $call->status;
            $call_array['start_time'] = Carbon::parse($call->startTime)->toDateTimeString();
            $call_array['end_time'] = Carbon::parse($call->endTime)->toDateTimeString();
            $call_array['duration'] = $call->duration;
            $call_array['direction'] = $call->direction; // inbound or outbound

            if ($call->price) {
                $call_array['price'] = $call->price . " " . $call->priceUnit;
            } elseif ($call->price == null) {
                $call_array['price'] = 0;
            }

            return $this->success("Call SID detail", $call_array);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    public function create_call (Request $request) {
        // Get .env settings
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $appSid = getenv("TWILIO_TWIML_APP_SID");

        $capability = new ClientToken($account_sid, $auth_token);
        $capability->allowClientOutgoing($appSid);
        $capability->allowClientIncoming('18647148794');
        $token = $capability->generateToken();

        $create_call = Call::create([
            'twilio_call_sid' => $request->input('CallSid'),
            'from_number' => $request->input('Caller'),
            'to_number' => $request->input('Called'),
            'duration' => $request->input('CallDuration'),
            'type' => $request->input('Direction') // inbound or outbound
        ]);

        // Get .env settings
        $get_env_settings = new GetEnvSettings();
        $client = $get_env_settings->getEnvSettings();

        $call = $client->calls($create_call->twilio_call_sid)->fetch();

        $get_call = Call::where('twilio_call_sid', '=', $call->sid)->first();
        $get_call->start_time = Carbon::parse($call->startTime)->toDateTimeString();
        $get_call->end_time = Carbon::parse($call->endTime)->toDateTimeString();
        $get_call->save();

        return response(file_get_contents(public_path('response.xml')), 200, ['Content-Type' => 'text/xml']);
    }

    public function make_outbound_call (Request $request) {
        $inputs = ['to_number' => $request->to_number]; // Where to make a voice call (your cell phone?)
        $rules = ['to_number' => 'required'];
        $validation = Validator::make($inputs, $rules);

        if ($validation->fails()) return $this->error($validation->errors()->all());

        // Get .env settings
        $get_env_settings = new GetEnvSettings();
        $client = $get_env_settings->getEnvSettings();
        $app_url = getenv("APP_URL");
        $twilio_number = getenv("TWILIO_NUMBER");

        $create_call = $client->account->calls->create(
            $request->to_number,
            $twilio_number, // A Twilio number you own with Voice capabilities
            [
                "method" => "GET",
                "statusCallback" => "$app_url/api/calls/create", // Production URL
                "statusCallbackMethod" => "POST",
                "url" => "http://demo.twilio.com/docs/voice.xml"
            ]
        );

        return $this->success("Outbound call is on the way");
    }

    public function receive_inbound_call (Request $request) {
        $create_call = Call::create([
            'twilio_call_sid' => $request->input('CallSid'),
            'from_number' => $request->input('Caller'),
            'to_number' => $request->input('Called'),
            'duration' => $request->input('CallDuration'),
            'type' => $request->input('Direction') // inbound or outbound
        ]);

        // Get .env settings
        $get_env_settings = new GetEnvSettings();
        $client = $get_env_settings->getEnvSettings();

        $call = $client->calls($create_call->twilio_call_sid)->fetch();

        $get_call = Call::where('twilio_call_sid', '=', $call->sid)->first();
        $get_call->start_time = Carbon::parse($call->startTime)->toDateTimeString();
        $get_call->end_time = Carbon::parse($call->endTime)->toDateTimeString();
        $get_call->save();

        $response = new VoiceResponse;

        // Read a message aloud to the caller
        $response->say("Thank you for calling! Have a great day.", array("voice" => "alice"));

        echo $response;
    }
}
