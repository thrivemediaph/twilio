<?php

namespace App\Repositories;

/* Models */
use App\Models\Recording;

/* Core */
use App\Interfaces\RecordingInterface;
use App\Services\TwilioService\GetEnvSettings;
use App\Traits\ResponseAPI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RecordingRepository implements RecordingInterface
{
    // Use ResponseAPI trait in this repository
    use ResponseAPI;

    public function create_recording (Request $request) {
        $recording = Recording::create([
            'twilio_recording_sid' => $request->input('RecordingSid'),
            'twilio_call_sid' => $request->input('CallSid'),
            'recording_url' => $request->input('RecordingUrl'),
            'start_time' => $request->input('RecordingStartTime'),
            'duration' => $request->input('RecordingDuration'),
        ]);

        return $this->success("Recording created!", $recording);
    }

    public function make_outbound_call_with_recording (Request $request) {
        $inputs = ['to_number' => $request->to_number]; // Where to make a voice call (your cell phone?)
        $rules = ['to_number' => 'required'];
        $validation = Validator::make($inputs, $rules);

        if ($validation->fails()) return $this->error($validation->errors()->all());

        // Get .env settings
        $get_env_settings = new GetEnvSettings();
        $client = $get_env_settings->getEnvSettings();
        $twilio_number = getenv("TWILIO_NUMBER");
        $app_url = getenv("APP_URL");

        $create_recording = $client->account->calls->create(
            $request->to_number,
            $twilio_number, // A Twilio number you own with Voice capabilities
            [
                "method" => "GET",
                "record" => True,
                "recordingStatusCallback" => "$app_url/api/recordings/create", // Production URL
                "recordingStatusCallbackMethod" => "POST",
                "statusCallback" => "$app_url/api/calls/create", // Production URL
                "statusCallbackMethod" => "POST",
                "url" => "http://demo.twilio.com/docs/voice.xml"
            ]
        );

        return $this->success("Outbound call with recording is on the way");
    }
}
