<?php

namespace App\Repositories;

/* Core */
use App\Interfaces\UserInterface;
use App\Traits\ResponseAPI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Twilio\Rest\Client;

class UserRepository implements UserInterface
{
    // Use ResponseAPI trait in this repository
    use ResponseAPI;

    private function sendMessage ($message, $recipients) {
        // Get .env settings
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number = getenv("TWILIO_NUMBER");

        // Instantiate Twilio client with credentials
        $client = new Client($account_sid, $auth_token);

        // Send SMS
        $client->messages->create($recipients, [
            'from' => $twilio_number,
            'body' => $message
        ]);
    }

    public function sendCustomMessage (Request $request) {
        $validatedData = $request->validate([
            'users' => 'required|array',
            'body' => 'required',
        ]);
        $recipients = $validatedData["users"];
        // iterate over the array of recipients and send a Twilio request for each
        foreach ($recipients as $recipient) {
            $this->sendMessage($validatedData["body"], $recipient);
        }

        return back()->with([
            "success" => "Custom notification message is on the way!"
        ]);
    }

    public function logout () {
        if (Auth::check()) {
            Auth::user()->token()->revoke();

            return $this->success("Logged out successfully!");
        } else {
            return $this->error("User is not logged in", 401);
        }
    }
}
