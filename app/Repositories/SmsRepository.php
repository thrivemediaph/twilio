<?php

namespace App\Repositories;

/* Models */
use App\Models\Answer;
use App\Models\Question;
use App\Models\Sms;
use App\Models\User;

/* Core */
use App\Helpers\StrPosX;
use App\Interfaces\SmsInterface;
use App\Services\TwilioService\GetEnvSettings;
use App\Traits\ResponseAPI;
use Illuminate\Http\Request;
use Twilio\TwiML\MessagingResponse;

class SmsRepository implements SmsInterface
{
    // Use ResponseAPI trait in this repository
    use ResponseAPI;

    public function receiveSMS (Request $request) {
        // WORKS! - SMS MESSAGE RECEIVED FROM TWLNET

        // Record the SMS received
        Sms::create([
            'sms_message_id' => $request->input('SmsMessageSid'),
            'sender_number' => $request->input('From'),
            'receiver_number' => $request->input('To'),
            'message' => $request->input('Body'),
            'no_of_segments' => $request->input('NumSegments')

        ]);

        // Set the content-type to XML to send back TwiML from the PHP Helper Library
        header("content-type: text/xml");

        $response = new MessagingResponse();
        $response->message("I'm using the Twilio PHP library to respond to this SMS!");

        echo $response;

        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $twilio = new Client($account_sid, $auth_token);

        $messages = $twilio->messages->read([], 1);
        $array = array();

        for ($i = 0; $i < count($messages); $i++) {
            $array['sms_message_id'] = $messages[$i]->sid;
            $array['sender_number'] = $messages[$i]->from;
            $array['receiver_number'] = $messages[$i]->to;
            $array['message'] = $messages[$i]->body;
            $array['no_of_segments'] = $messages[$i]->numSegments;
        }

        Sms::create([
            'sms_message_id' => $array['sms_message_id'],
            'sender_number' => $array['sender_number'],
            'receiver_number' => $array['receiver_number'],
            'message' => $array['message'],
            'no_of_segments' => $array['no_of_segments']
        ]);
    }

    public function listMessages () {
        // Get .env settings
        $get_env_settings = new GetEnvSettings();
        $client = $get_env_settings->getEnvSettings();

        $messages = $client->messages->read([], 1);
        $array = array();

        for ($i = 0; $i < count($messages); $i++) {
            $array['sms_message_id'] = $messages[$i]->sid;
            $array['sender_number'] = $messages[$i]->from;
            $array['receiver_number'] = $messages[$i]->to;
            $array['message'] = $messages[$i]->body;
            $array['no_of_segments'] = $messages[$i]->numSegments;
        }

        foreach ($messages as $record) {
            print($record->sid);
        }
        return $array;
    }

    public function respondToQuestion (Request $request) {
        $response = new MessagingResponse();
        // Check if sender is registered
        $sender = $request->input('From'); // Get user number
        $message = $request->input('Body'); // Get user message

        $user = User::where('mobile_number', '=', $sender)->first();

        if (!$user) {
            $response->message("You are not registered!");
            return $response;
        } else {
            $haystack = $message;
            $needle = '/';
            $str_posX = new StrPosX();

            // Check if code keyword is present in the message
            $get_position_of_code_keyword = $str_posX->strposX($haystack, $needle, 1);
            $code_substring = substr($haystack, 0, $get_position_of_code_keyword + 1);

            if ($code_substring != 'CODE/') {
                $response->message("Answering a question code requires the keyword CODE followed by /. eg. CODE/123456/MY ANSWER");
                return $response;
            }

            // Check if question code is correct
            $question_code_substring = substr($haystack, $get_position_of_code_keyword + 1, 6);

            $question = Question::where('code', '=', $question_code_substring)->first();

            if (!$question) {
                $response->message("Code not found or is invalid. Please recheck your question code and send again. Codes consist of 6 digits");
                return $response;
            }

            // Get the answer of the sender and save it to the Answers table
            $get_position_of_answer = $str_posX->strposX($haystack, $needle, 2);
            $answer_substring = substr($haystack, $get_position_of_answer + 1);

            Answer::create([
                'question_code' => $question_code_substring,
                'full_name' => $user->full_name,
                'mobile_number' => $sender,
                'answer' => $answer_substring,
                'message' => $message
            ]);

            $response->message("Hello, " .$user->mobile_number. "! Thank you for participating in our survey for question code " .$question_code_substring);
            return $response;
        }
    }
}
