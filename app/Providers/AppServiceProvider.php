<?php

namespace App\Providers;

use Illuminate\Cache\NullStore;
use Cache;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //check that app is local
        if ($this->app->isLocal()) {
            //if local register your services you require for development
//            $this->app->register('Barryvdh\Debugbar\ServiceProvider');
        } else {
            //else register your services you require for production
            $this->app['request']->server->set('HTTPS', true);
        }
        $this->app->bind(
            'App\Interfaces\CallInterface',
            'App\Repositories\CallRepository'
        );
        $this->app->bind(
            'App\Interfaces\RecordingInterface',
            'App\Repositories\RecordingRepository'
        );
        $this->app->bind(
            'App\Interfaces\SmsInterface',
            'App\Repositories\SmsRepository'
        );
        $this->app->bind(
            'App\Interfaces\UserInterface',
            'App\Repositories\UserRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Cache::extend('none', function ($app) {
            return Cache::repository(new NullStore);
        });
    }
}
