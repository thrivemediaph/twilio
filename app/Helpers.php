<?php

namespace App\Helpers;

/* Models */
use App\Models\Question;

/**
 * Class GenerateRandomIntegers
 * @package Helpers
 *
 */
class GenerateRandomIntegers
{
    // Global variables
    public $int_start;
    public $int_end;
    public $length;
    public $generated_codes_list = array();

    public function __construct($int_start, $int_end, $length) {
        $this->int_start = (int) $int_start;
        $this->int_end = (int) $int_end;
        $this->length = (int) $length;
    }

    /**
     * @return int
     */
    public function generate() {
        $code = '';

        // Generate random integers between 1 - 9
        for ($i = 0; $i < $this->length; $i++) {
            $code .= rand($this->int_start, $this->int_end);
        }

        // Check if verification code already exists in the otp table
        $find_verification_code = Question::where('code', '=', $code)->first();

        // Debugging while loop counters and testing. Please do not remove this yet.
        // $this->generated_codes($code);
        $new_code = '';

        // If code exists, generate a new one.
        if ($find_verification_code) {
            $will_generate = True;

            while ($will_generate == True) { // Loop while this condition is met
                $new_code = ''; // Reset value before concatenating
                // Generate another code using random integers
                for ($i = 0; $i < $this->length; $i++) {
                    $new_code .= rand($this->int_start, $this->int_end);
                }
                $find_verification_code_if_exists = Question::where('code', '=', $new_code)->first();

                // If verification code does not exist, exit the while loop
                if (!$find_verification_code_if_exists) {
                    $will_generate = False;
                }
                // $this->generated_codes($new_code); // Push generated codes into array for debugging
            }
            $code = $new_code; // Use newly generated code as final code
        }
        return $code;
    }

    public function generated_codes($code_to_be_pushed) {
        array_push($this->generated_codes_list, $code_to_be_pushed);

        return $this->generated_codes_list;
    }
}

/**
 * Class StrPosX
 * An improvement over the original strpos() function found at
 * https://www.php.net/manual/en/function.strpos.php
 * @package Helpers
 *
 */
class StrPosX
{
    public function strposX($haystack, $needle, $number) {
        if ($number == '1') {
            return strpos($haystack, $needle);
        } elseif ($number > '1') {
            return strpos($haystack, $needle, $this->strposX($haystack, $needle, $number - 1) + strlen($needle));
        } else {
            return error_log("Error: Value for parameter $number is out of range");
        }
    }
}
