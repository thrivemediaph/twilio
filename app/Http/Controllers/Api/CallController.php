<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\CallInterface;
use Illuminate\Http\Request;

class CallController extends Controller
{
    protected $call_interface;

    public function __construct (CallInterface $call_interface) {
        $this->call_interface = $call_interface;
    }

    /** Get all calls
     *
     * @method  GET        api/calls/
     */
    public function get_call_list () {
        return $this->call_interface->get_call_list();
    }

    /** Get call by CallSid
     *
     * @param   CallSid    $call_sid
     * @method  GET        api/calls/{call_sid}
     */
    public function get_call_by_callsid ($call_sid) {
        return $this->call_interface->get_call_by_callsid($call_sid);
    }

    /** Create call
     *
     * @method  POST        api/calls/create
     */
    public function create_call (Request $request) {
        return $this->call_interface->create_call($request);
    }

    /** Make outbound call to specified phone number
     *
     * @method  POST        api/calls/outbound
     */
    public function make_outbound_call (Request $request) {
        return $this->call_interface->make_outbound_call($request);
    }

    /** Output XML response when number receives an incoming call
     *
     * @method  GET        api/calls/receive/inbound
     */
    public function receive_inbound_call (Request $request) {
        return $this->call_interface->receive_inbound_call($request);
    }
}
