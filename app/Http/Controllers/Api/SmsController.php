<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\SmsInterface;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    protected $sms_interface;

    public function __construct (SmsInterface $sms_interface) {
        $this->sms_interface = $sms_interface;
    }

    /** Respond to SMS using Twilio - called on webhook from Twilio account
     *
     * @param   Request     $request
     * @method  POST        api/sms/respond
     */
    public function receiveSMS (Request $request) {
        return $this->sms_interface->receiveSMS($request);
    }

    /** List all messages
     *
     * @method  GET        api/sms/list/all
     */
    public function listMessages () {
        return $this->sms_interface->listMessages();
    }

    /** User sends an sms for his full name and Twilio responds that user exists
     *
     * @method  GET        api/sms/question/respond
     */
    public function respondToQuestion (Request $request) {
        return $this->sms_interface->respondToQuestion($request);
    }
}
