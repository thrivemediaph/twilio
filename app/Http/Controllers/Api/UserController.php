<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\UserInterface;

class UserController extends Controller
{
    protected $user_interface;

    public function __construct (UserInterface $user_interface) {
        $this->user_interface = $user_interface;
    }
}
