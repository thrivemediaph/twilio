<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\RecordingInterface;
use Illuminate\Http\Request;

class RecordingController extends Controller
{
    protected $recording_interface;

    public function __construct (RecordingInterface $recording_interface) {
        $this->recording_interface = $recording_interface;
    }

    /** Create recording
     *
     * @method  POST        api/recordings/create
     */
    public function create_recording (Request $request) {
        return $this->recording_interface->create_recording($request);
    }

    /** Make outbound call with recording to specified phone number
     *
     * @method  POST        api/recordings/calls/outbound
     */
    public function make_outbound_call_with_recording (Request $request) {
        return $this->recording_interface->make_outbound_call_with_recording($request);
    }
}
