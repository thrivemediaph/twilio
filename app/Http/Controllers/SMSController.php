<?php

namespace App\Http\Controllers;

/* Models */
use App\Models\Question;
use App\Models\Sms;
use App\Models\User;
use App\Models\UsersPhoneNumber;

/* Core */
use App\Helpers\GenerateRandomIntegers;
use App\Services\TwilioService\GetEnvSettings;
use Illuminate\Http\Request;

class SMSController extends Controller
{
    public function show() {
        $all_sms = Sms::all();

        return view('view_sms_logs', ["sms" => $all_sms]); // Return view with data
    }

    public function sendMessageForm() {
        $users = User::all(); // Query db with model

        return view('send_sms', ["users" => $users]); // Return view with data
    }

    public function sendCustomMessage(Request $request) {
        $validatedData = $request->validate([
            'users' => 'required|array',
            'body' => 'required',
            'add_question' => 'nullable' // Checkbox
        ]);

        if (array_key_exists('add_question', $validatedData) == true) { // If the checkbox is checked
            $generate_random_integers = new GenerateRandomIntegers(1, 9, 6);
            $code = $generate_random_integers->generate();

            Question::create([
                'code' => $code,
                'question' => $validatedData['body']
            ]);
        }
        $recipients = $validatedData["users"];

        // Get .env settings
        $get_env_settings = new GetEnvSettings();
        $client = $get_env_settings->getEnvSettings();
        $twilio_number = getenv("TWILIO_NUMBER");

        foreach ($recipients as $recipient) { // Iterate over the array of recipients and send a Twilio request for each
            // Send SMS
            $send = $client->messages->create($recipient, [
                'from' => $twilio_number,
                'body' => $validatedData['body']
            ]);

            Sms::create([
                'sms_message_id' => $send->sid,
                'sender_number' => $send->from,
                'receiver_number' => $recipient,
                'message' => $send->body,
                'no_of_segments' => $send->numSegments
            ]);
        }

        return back()->with(["success" => "Custom SMS is on the way!"]);
    }
}
