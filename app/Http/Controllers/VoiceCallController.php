<?php

namespace App\Http\Controllers;

/* Models */
use App\Models\Call;
use App\Models\Recording;

/* Core */
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use function PHPUnit\Framework\returnSelf;
use Twilio\TwiML\Voice\Record;

class VoiceCallController extends Controller
{
    public function show() {
        $calls = Call::all();
        $recordings = Recording::all();
        $calls_array = array();

        for ($i = 0; $i < $calls->count(); $i++) {
            $calls_array[$i]['call_id'] = $calls[$i]->id;
            $calls_array[$i]['twilio_call_sid'] = $calls[$i]->twilio_call_sid;
            $calls_array[$i]['from_number'] = $calls[$i]->from_number;
            $calls_array[$i]['to_number'] = $calls[$i]->to_number;
            $calls_array[$i]['start_time'] = $calls[$i]->start_time;
            $calls_array[$i]['end_time'] = $calls[$i]->end_time;
            $calls_array[$i]['duration'] = $calls[$i]->duration;
            $calls_array[$i]['type'] = $calls[$i]->type;

            $recording = Recording::where('twilio_call_sid', '=', $calls[$i]->twilio_call_sid)->first();
            if ($recording) {
                $calls_array[$i]['recording_id'] = $recording->id;
                $calls_array[$i]['twilio_recording_sid'] = $recording->twilio_recording_sid;
                $calls_array[$i]['recording_url'] = $recording->recording_url;
                $calls_array[$i]['recording_start_time'] = $recording->start_time;
                $calls_array[$i]['recording_duration'] = $recording->duration;
            } else {
                $calls_array[$i]['twilio_recording_sid'] = null;
                $calls_array[$i]['recording_url'] = null;
                $calls_array[$i]['recording_start_time'] = null;
                $calls_array[$i]['recording_duration'] = null;
            }
        }

        return view('view_call_logs', ["calls" => $calls_array]); // Return view with data
    }

    public function voiceCall(){
        return view('call');
    }
}
