<?php

namespace App\Services\TwilioService;

/* Models */
use Twilio\Rest\Client;

/**
 * Class GetEnvSettings
 * @package Services
 *
 */
class GetEnvSettings
{
    public function getEnvSettings() {
        // Get .env settings
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");

        // Instantiate Twilio client with credentials
        $client = new Client($account_sid, $auth_token);

        return $client;
    }
}
