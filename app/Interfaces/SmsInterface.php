<?php

namespace App\Interfaces;

use Illuminate\Http\Request;

interface SmsInterface
{
    public function receiveSMS (Request $request);

    public function listMessages ();

    public function respondToQuestion (Request $request);
}
