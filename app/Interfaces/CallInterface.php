<?php

namespace App\Interfaces;

use Illuminate\Http\Request;

interface CallInterface
{
    public function get_call_list ();

    public function get_call_by_callsid ($call_sid);

    public function create_call (Request $request);

    public function make_outbound_call (Request $request);

    public function receive_inbound_call (Request $request);
}
