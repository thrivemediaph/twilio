<?php

namespace App\Interfaces;

use Illuminate\Http\Request;

interface RecordingInterface
{
    public function create_recording (Request $request);

    public function make_outbound_call_with_recording (Request $request);
}
